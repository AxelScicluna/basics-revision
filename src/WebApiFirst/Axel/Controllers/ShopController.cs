﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using Axel.Models;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Axel.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ShopController : ApiController
    {

 
        private Shop[] items = new Shop[]
   {
        new Shop { id = 1, name = "Haleemah Redfern", price = 5.40,  inStock = true},
        new Shop { id = 2, name = "Tacos", price = 1.00,  inStock = true},
        new Shop { id = 3, name = "Plant", price = 104.10,  inStock = false},
        new Shop { id = 4, name = "Mystery Meat", price = 40.20,  inStock = true},
   };


        // GET: api/Shop
      
        public IEnumerable<Shop> Get()
        {
            return items;
        }

        // GET: api/Shop/5
        public int Get(int id)
        {
            return items.FirstOrDefault((p) => p.id == id).id;
        }

        // POST: api/Shop
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Shop/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Shop/5
        public void Delete(int id)
        {
        }
    }
}
