﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Axel.Models
{
    public class Shop
    {
        public int id { get; set; }
        public string name { get; set; }
        public double price { get; set; }
        public bool inStock { get; set; }
    }
}